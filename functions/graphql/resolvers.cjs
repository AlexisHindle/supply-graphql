/* eslint-disable max-len */
const admin = require("firebase-admin");
admin.initializeApp();
const db = admin.firestore();
db.settings({
  ignoreUndefinedProperties: true,
});
exports.resolverFunctions = {
  Query: {
    user: (_, {email}) => {
      return new Promise((resolve, reject) => {
        db.collection("users").where("email", "==", email)
            .get()
            .then((querySnapshot) => {
              const items = [];
              querySnapshot.forEach((doc) => {
                items.push(doc.data());
                resolve(doc.data());
              });
            })
            .catch((error) => {
              console.error(
                  "Error getting documents when finding a user:", error);
            });
      });
    },
    users: () => {
      return new Promise((resolve, reject) => {
        fetchAllUsers((data) => {
          resolve(data);
        });
      });
    },
    company: (_, {id}) => {
      return new Promise((resolve, reject) => {
        db.collection("companies")
            .where("id", "==", id)
            .get()
            .then((querySnapshot) => {
              const items = [];
              querySnapshot.forEach((doc) => {
                items.push(doc.data());
                resolve(doc.data());
              });
            })
            .catch(function(error) {
              console.log("Error getting documents: ", error);
            });
      });
    },
    companies: () => {
      return new Promise((resolve, reject) => {
        fetchAllCompanies((data) => {
          resolve(data);
        });
      });
    },
    review: (_, {id}) => {
      return new Promise((resolve, reject) => {
        db.collection("reviews")
            .where("id", "==", id)
            .get()
            .then((querySnapshot) => {
              const items = [];
              querySnapshot.forEach((doc) => {
                items.push(doc.data());
                resolve(doc.data());
              });
            });
      });
    },
    reviews: () => {
      return new Promise((resolve, reject) => {
        fetchAllReviews((data) => {
          resolve(data);
        });
      });
    },
    itemToShare: (_, {id}) => {
      return new Promise((resolve, reject) => {
        db.collection("sharedItems")
            .where("id", "==", id)
            .get()
            .then((querySnapshot) => {
              const items = [];
              querySnapshot.forEach((doc) => {
                items.push(doc.data());
                resolve(doc.data());
              });
            });
      });
    },
    itemsToShare: () => {
      return new Promise((resolve, reject) => {
        fetchAllItemsToShare((data) => {
          resolve(data);
        });
      });
    },
    messagesByUserId(_root, id, name) {
      // console.log(id, name);
      return new Promise((resolve, reject) => {
        const items = [];
        db.collection("messages").get().then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            if (doc.data().users.some((user) => id.userId === user.id)) {
              items.push(doc.data());
            }
          });
        }).then(() => resolve(items))
            .catch((err) => console.log(err));
        // fetchMessageByUserId((data) => {
        //   data.then((d) => console.log(d.data()));
        //   // const items = [];
        //   // data.forEach((item) => {
        //   //   if (item.users.some((user) => id.userId === user.id)) {
        //   //     items.push(item);
        //   //   }
        //   // });
        //   // console.log(items, "=========");
        //   // resolve(items);

        //   // items.forEach((item) => {
        //   //   console.log(item, "===== item ==== ");
        //   //   resolve(item);
        //   // });
        // });
        // const query = db.collection("messages");
        // // query = query.where("receivedById", "==", id.userId);
        // // query = query.where("sentBy", "==", id.name);
        // query.get()
        //     .then((querySnapshot) => {
        //       const items = [];
        //       querySnapshot.forEach((doc) => {
        //         // console.log(doc.data(), "===== data ====");
        //         items.push(doc.data());
        //         const messagesToReturn = doc.data().users.some((user) => id.userId === user.id);
        //         if (messagesToReturn) {
        //           resolve(items);
        //         } else {
        //           resolve("No message can be returned");
        //         }
        //       });
        //     });
      });
    },
    messagesByThreadId(_root, id) {
      return new Promise((resolve, reject) => {
        console.log(id);

        db.collection("messages")
            .where("threadId", "==", id.threadId)
            .get()
            .then((querySnapshot) => {
              const items = [];
              querySnapshot.forEach((doc) => {
                items.push(doc.data());
                resolve(items);
              });
            });
      });
    },
  },

  Mutation: {
    async createUser(_root, {input}) {
      const ref = db.collection("users").doc();
      const userToCreate = {
        id: ref.id,
        firstName: input.firstName,
        lastName: input.lastName,
        email: input.email,
        userType: input.userType,
        companyName: input.companyName,
        userUid: input.userUid,
      };
      await ref.set(userToCreate);
      return userToCreate;
    },
    async updateUser(_root, {id, input}) {
      const ref = db.collection("users").doc(id);
      await ref.update(input);
      return input;
    },
    async deleteUserAndAssociated(_root, {id}) {
      // const user = admin.auth().getUser("5hRxvhNy4WSi2nQEK9nZVmSGHwq2");
      db.collection("users")
          .where("id", "==", id)
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((user) => {
              admin.auth().deleteUser(user.data().uid).then(() => console.log("user deleted"));
              if (user.data().companyName) {
                db.collection("companies")
                    .where("companyName", "==", user.data().companyName)
                    .get()
                    .then((querySnapshot) => {
                      querySnapshot.forEach((company) => {
                        const companyRef = db.collection("companies").doc(company.data().id);
                        companyRef.delete().then(() => console.log("Company Deleted"));
                      });
                    });

                db.collection("reviews")
                    .where("companyName", "==", user.data().companyName)
                    .get()
                    .then((querySnapshot) => {
                      querySnapshot.forEach((review) => {
                        const reviewRef = db.collection("reviews").doc(review.data().id);
                        reviewRef.delete().then(() => "Review deleted");
                      });
                    });
              } else {
                console.log("user has no company associated");
              }
              const userRef = db.collection("users").doc(user.data().id);
              userRef.delete().then(() => console.log("User deleted"));
            });
          });
      return "Deleted user, company and reviews";
    },
    async deleteUser(_root, {id}) {
      const ref = db.collection("users").doc(id);
      await ref.delete();
      return "Deleted user";
    },
    async createCompany(_root, {input}) {
      const ref = db.collection("companies").doc();
      const companyToCreate = {
        id: ref.id,
        companyName: input.companyName,
        description: input.description,
        postcode: input.postcode,
        type: input.type,
        category: input.category,
        county: input.county,
        icon: input.icon,
        lat: input.lat,
        lng: input.lng,
        logo: input.logo,
        email: input.email,
        website: input.website,
        facebook: input.facebook,
        twitter: input.twitter,
        instagram: input.instagram,
        membership: input.membership,
        shopOnline: input.shopOnline,
        hasShop: input.hasShop,
        organic: input.organic,
        image: input.image,
        tags: input.tags,
        reviews: input.reviews,
      };
      await ref.set(companyToCreate);
      return companyToCreate;
    },
    async updateCompany(_root, {id, input}) {
      const ref = db.collection("companies").doc(id);
      await ref.update(input);
      return input;
    },
    async deleteCompany(_root, {id}) {
      const ref = db.collection("companies").doc(id);
      await ref.delete();
      return "Deleted company";
    },
    async createItemToShare(_root, {input}) {
      const ref = db.collection("sharedItems").doc();
      const itemToCreate = {
        id: ref.id,
        itemName: input.itemName,
        username: input.username,
        description: input.description,
        useByDate: input.useByDate,
        image: input.image,
        pickUpTime: input.pickUpTime,
        pickUpDescription: input.pickUpDescription,
        lat: input.lat,
        lng: input.lng,
        dateAdded: input.dateAdded,
        quantity: input.quantity,
        userId: input.userId,
        address: input.address,
      };
      await ref.set(itemToCreate);
      return itemToCreate;
    },
    async updateItemToShare(_root, {id, input}) {
      const ref = db.collection("sharedItems").doc(id);
      await ref.update(input);
      return input;
    },
    async deleteItemToShare(_root, {id}) {
      const ref = db.collection("sharedItems").doc(id);
      await ref.delete();
      return "Deleted shared item";
    },
    async sendItemRequest(_root, {input}) {
      const ref = db.collection("messages").doc();
      const message = {
        "id": ref.id,
        "created": input.created,
        "users": input.users,
        "messages": input.messages,
      };
      await ref.set(message);
      return message;
    },
    async updateItemRequest(_root, {id, input}) {
      console.log(input);
      const ref = db.collection("messages").doc(id);
      ref.get().then((doc) => {
        if (doc.exists) {
          ref.update(input);
        } else {
          console.log("No such document!");
          return "No such document!";
        }
      }).catch((error) => {
        console.log("Error getting document:", error);
        return `Error getting document:", ${error}`;
      });
      // await ref.update(input);
      return input;
    },
  },

  User: {
    company: (user) => {
      return new Promise((resolve, reject) => {
        fetchAllCompanies((data) => {
          const array = [];
          data.map((item) => {
            if (item.companyName.toLowerCase() ===
            user.companyName.toLowerCase()) {
              array.push(item);
            }
          });
          console.log(...array);
          resolve(...array);
        });
      });
    },
    itemsToShare: (user) => {
      return new Promise((resolve, reject) => {
        fetchAllItemsToShare((data) => {
          const array = [];
          data.map((item) => {
            if (item.userId ===
            user.id) {
              array.push(item);
            }
          });
          resolve(array);
        });
      });
    },
  },

  itemToShare: {
    user: (itemToShare) => {
      return new Promise((resolve, reject) => {
        fetchAllUsers((data) => {
          const array = [];
          data.map((item) => {
            if (item.id === itemToShare.userId) {
              array.push(item);
            }
          });
          resolve(array);
        });
      });
    },
  },

  Company: {
    reviews: (company) => {
      return new Promise((resolve, reject) => {
        fetchAllReviews((data) => {
          const array = [];
          data.map((item) => {
            if (item.companyName.toLowerCase() ===
            company.companyName.toLowerCase()) {
              array.push(item);
            }
          });
          resolve(array);
        });
      });
    },
    // TODO: Add itemsToShare to companies
  },
};

const fetchAllUsers = async (callback) => {
  const users = await db.collection("users").get();
  return callback(users.docs.map((doc) => doc.data()));
};

const fetchAllCompanies = async (callback) => {
  const companies = await db.collection("companies").get();
  const companiesArray = companies.docs.map((doc) => doc.data());
  return callback(companiesArray);
};

const fetchAllReviews = async (callback) => {
  const reviews = await db.collection("reviews").get();
  return callback(reviews.docs.map((doc) => doc.data()));
};

const fetchAllItemsToShare = async (callback) => {
  const sharedItems = await db.collection("sharedItems").get();
  return callback(sharedItems.docs.map((doc) => doc.data()));
};

// const fetchMessageByUserId = async (callback) => {
//   const user = await db.collection("messages").get();
//   return callback(user.docs);
// };
