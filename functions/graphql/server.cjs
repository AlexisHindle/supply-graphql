/* eslint-disable require-jsdoc */
const express = require("express");
const {ApolloServer} = require("apollo-server-express");

const schemas = require("./schema.cjs");
const res = require("./resolvers.cjs");

exports.gqlServer = function gqlServer() {
  const app = express();
  const resolvers = res.resolverFunctions;
  const typeDefs = schemas.schema;

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    introspection: true,
    playground: true,
  });
  server.start().then(() => {
    server.applyMiddleware({app, path: "/", cors: true});
  });

  return app;
};

