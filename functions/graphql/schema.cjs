const {gql} = require("apollo-server-express");

exports.schema = gql`  
  type Query {
    user(email: String!): User
    users: [User]
  }

  type Mutation {
    createUser(input: createUserInput!): User
  }

  type Mutation {
    updateUser(id: ID!, input: createUserInput!): User
  }

  type Mutation {
    deleteUser(id: ID!): String
  }

  type Mutation {
    deleteUserAndAssociated(id: ID!): String
  }
  
  input createUserInput {
    id: ID
    firstName: String!
    lastName: String!
    email: String!
    userType: String!
    companyName: String
    userUid: String
  }

  type User {
    id: ID
    firstName: String
    lastName: String
    email: String
    userType: String
    companyName: String
    company: Company
    userUid: String
    itemsToShare: [itemToShare]
  }

  type Query {
    company(id: ID!): Company
    companies: [Company]
  }

  type Mutation {
    createCompany(input: createCompanyInput!): Company
  }

  type Mutation {
    updateCompany(id: ID!, input: createCompanyInput!): Company
  }

  type Mutation {
    deleteCompany(id: ID!): String
  }

  input createCompanyInput {
    id: ID
    companyName: String
    description: String
    postcode: String
    type: [String]
    category: [String]
    county: String
    icon: String
    lat: String
    lng: String
    logo: String
    email: String
    website: String
    facebook: String
    twitter: String
    instagram: String
    membership: Boolean
    shopOnline: Boolean
    hasShop: Boolean
    organic: Boolean
    image: String
    tags: [String]
  }

  type Company {
    id: ID
    companyName: String
    description: String
    postcode: String
    type: [String]
    category: [String]
    county: String
    icon: String
    lat: String
    lng: String
    logo: String
    email: String
    website: String
    facebook: String
    twitter: String
    instagram: String
    membership: Boolean
    shopOnline: Boolean
    hasShop: Boolean
    organic: Boolean
    image: String
    tags: [String]
    reviews: [Review]
  }

  type Query {
    review(id: ID!): Review
    reviews: [Review]
  }

  type Review {
    id: ID
    companyName: String!
    rating: Int
    author: String
    review: String
    date: String
  }

  type Query {
    itemToShare(id: ID!): itemToShare
    itemsToShare: [itemToShare]
  }

  type itemToShare {
    id: ID
    itemName: String
    description: String
    userId: String
    username: String
    dateAdded: String
    quantity: Int
    pickUpInformation: String
    image: String
    lat: String
    lng: String
    useByDate: String
    address: String
    user: [User]
    type: String
  }

  type Mutation {
    createItemToShare(input: createItemToShareInput!): itemToShare
  }

  type Mutation {
    updateItemToShare(id: ID!, input: createItemToShareInput!): itemToShare
  }

  type Mutation {
    deleteItemToShare(id: ID!): String
  }

  input createItemToShareInput {
    id: ID
    itemName: String
    description: String
    userId: String
    username: String
    pickUpInformation: String
    dateAdded: String
    quantity: Int
    image: String
    lat: String
    lng: String
    useByDate: String
    address: String
    type: String
  }

  type Query {
    message(id: ID!): Message
    messagesByUserId(userId: ID!, name: String): [Conversation],
    messagesByThreadId(threadId: ID!): [Message],
  }

  type Message {
    threadId: ID
    message: String
    sender: Sender
    created: String
    read: Boolean
    itemName: String
    # userId: String
    # sentById: String
    # sentBy: String
    # receivedById: String
    # receivedBy: String
    # dateSent: String
    # message: String
    # itemId: String
    # itemName: String
    # threadId: String
    # read: Booleancreated: Date.now(),
  }

  input MessageInput {
    threadId: ID
    message: String
    sender: SenderInput
    created: String
    read: Boolean
    itemName: String
  }

  type Conversation {
    id: ID
    users: [Sender]
    messages: [Message],
  }

  type Mutation {
    sendItemRequest(input: sendItemRequestInput!): Conversation
  }

  type Mutation {
    updateItemRequest(id: ID!, input: sendItemRequestInput!): Conversation
  }

  type Sender {
    firstName: String,
    lastName: String,
    id: String,
    readMessages: [Boolean]
  }

  input SenderInput {
    firstName: String,
    lastName: String,
    id: String,
    readMessages: [Boolean]
  }

  input sendItemRequestInput {
    id: ID
    users: [SenderInput]
    messages: [MessageInput],
    sender: SenderInput
    created: String
    read: Boolean
    # id: ID,
    # userId: String
    # sentById: String
    # sentBy: String
    # receivedById: String
    # receivedBy: String
    # dateSent: String
    # message: String
    # itemId: String
    # itemName: String
    # threadId: String
    # read: Boolean
  }
`;
