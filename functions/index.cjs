// import * as functions from "firebase-functions";
const functions = require("firebase-functions");
const gqlServer = require("./graphql/server.cjs");
const server = gqlServer.gqlServer();
// Graphql api
// https://us-central1-<project-name>.cloudfunctions.net/api/
exports.graphql = functions.https.onRequest(server);
